<?php

require_once("/vagrant/backend/vendor/autoload.php");

use imagekeeper\components\App;

$app = App::getInstance();
$app->init();
$app->run();
