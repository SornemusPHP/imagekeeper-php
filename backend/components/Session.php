<?php

namespace imagekeeper\components;

class Session
{
    //private $sessionData;

    public function __construct(){
        //$this->sessionData = [];
    }

    public function getValue($name){
        return $_SESSION[$name];
    }

    public function setValue($name, $value){
        $_SESSION[$name] = $value;
    }

    public function getAll(){
        return $_SESSION;
    }

    public function startSession(){
        session_start();
    }

    public function endSession(){
        session_write_close();
    }
}