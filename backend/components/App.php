<?php

namespace imagekeeper\components;

use imagekeeper\components\Logger;
use imagekeeper\components\Session;
use imagekeeper\components\UrlRouter;
use Redis;

class App
{
    /**
     * @var App
     */
    public static $instance = null;

    /**
     * @var array
     */
    private $config = null;
    /**
     * @var \imagekeeper\components\Logger
     */
    private $logger = null;
    /**
     * @var \imagekeeper\components\Session
     */
    private $session = null;
    /**
     * @var \imagekeeper\components\UrlRouter
     */
    private $router = null;



    /**
     * @var \MongoDb\Database
     */
    private $mongoDb = null;

    /**
     * @var Redis
     */
    private $redis = null;

    /**
     * @var bool
     */
    private $init = false;


    public function init(){
        $this->config = require_once("/vagrant/backend/config/App.php");

        $this->logger = new Logger();

        $this->session = new Session();

        $this->router = new UrlRouter();



        $this->mongoDb = (new \MongoDB\Client)->local;

        $this->redis = new Redis();




        $this->init = true;
    }

    public function run(){
        if(!$this->init){
            die("Fatal error: App was not initialized");
        }

        $this->session->startSession();

        $url = trim($_SERVER['REQUEST_URI'], '/');
        $this->router->route($url);





/*
        $bucket = $mongo->example->selectGridFSBucket();
        $file = fopen('apple-touch-icon.png', 'rb');
        $res = $bucket->uploadFromStream('apple-touch-icon.png', $file);
        var_dump($res);

        $log->info($res->oid);

        mkdir("./img", 0777);
        $img = fopen('./img/' . $res->oid . '.png', 'wb');
        $bucket->downloadToStream($res, $img);

        echo "<img src='./images/newimg.png'/>";
*/
    }

    /**
     * @return array
     */
    public function getConfig(){
        return $this->config;
    }

    /**
     * @return \imagekeeper\components\Logger
     */
    public function getLogger(){
        return $this->logger;
    }

    /**
     * @return \imagekeeper\components\Session
     */
    public function getSession(){
        return $this->session;
    }

    /**
     * @return \MongoDb\Database
     */
    public function getMongoDb(){
        return $this->mongoDb;
    }

    /**
     * @return Redis
     */
    public function getRedis(){
        return $this->redis;
    }

    public static function getInstance(){
        if (!self::$instance){
            self::$instance = new App();
        }
        return self::$instance;
    }
}
