<?php

namespace imagekeeper\components;


use DateTime;

class Logger
{
    public static $LOG_LEVELS = [
        'DEBUG' => 100,
        'INFO' => 200,
        'WARN' => 300,
        'ERROR' => 400,
    ];

    private $formattedParams = [
        'time' => '',
        'level' => '',
        'context' => [],
        'message' => '',
    ];

    /**
     * @var array
     */
    private $config;

    public function __construct()
    {
        $this->config = App::getInstance()->getConfig()['logger'];
    }



    public function debug(string $message){
        $this->log($message, 'DEBUG');
    }

    public function info(string $message){
        $this->log($message, 'INFO');
    }

    public function warn(string $message){
        $this->log($message, 'WARN');
    }

    public function error(string $message){
        $this->log($message, 'ERROR');
    }

    public function log(string $message, string $level = "INFO"){
        $now = DateTime::createFromFormat('U.u', microtime(true));
        $this->formattedParams['time'] = $now->format("Y-m-d H:i:s") . '.' .
            substr($now->format("u"), 0, 3);


        $this->formattedParams['level'] = $level;


        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1];
        $filepath = explode('/', $backtrace['file']);
        $file = end($filepath);
        $this->formattedParams['context']['file'] = $file;
        $this->formattedParams['context']['line'] = $backtrace['line'];

        $this->formattedParams['message'] = $message;



        //TODO: check the level of each logger from config
        $this->logToFile();
        $this->logToMongo();
    }

    private function logToFile(){
        $context = $this->formattedParams['context']['file'];
        if(strlen($context) > 25){
            $context = substr($context, 0, 23);
        }
        $context = str_pad($context, 25, '.', STR_PAD_RIGHT);
        $context .= $this->formattedParams['context']['line'];

        $level = str_pad($this->formattedParams['level'], 5, ' ', STR_PAD_RIGHT);




        $formattedMessage = $this->config['file']['format'];
        $formattedMessage = str_replace('%TIME%', $this->formattedParams['time'], $formattedMessage);
        $formattedMessage = str_replace('%CONTEXT%', $context, $formattedMessage);
        $formattedMessage = str_replace('%LEVEL%', $level, $formattedMessage);
        $formattedMessage = str_replace('%MESSAGE%', $this->formattedParams['message'], $formattedMessage);
        $formattedMessage .= "\n";

        file_put_contents($this->config['file']['path'], $formattedMessage, FILE_APPEND);
    }

    private function logToMongo(){
        $context = $this->formattedParams['context']['file'] . '::' . $this->formattedParams['context']['line'];

        $document = [
            'time' => $this->formattedParams['time'],
            'context' => $context,
            'level' => $this->formattedParams['level'],
            'message' => $this->formattedParams['message']
        ];


        $collectionName = $this->config['mongo']['collection'];
        $collection = App::getInstance()->getMongoDb()->$collectionName;
        $collection->insertOne($document);
    }
}
