<?php

namespace imagekeeper\components;


class Redis
{
    /**
     * @var \Redis
     */
    private $redis = null;
    
    /**
     * @var string
     */
    private $ip;
    
    /**
     * @var string
     */
    private $port;
    
    
    
    public function __construct()
    {
        $this->redis = new \Redis();
        $this->ip = '127.0.0.1';
        $this->port = '6379';
    }
    /*
    public function __call($name, $arguments)
    {
        
        is_callable($this->redis->$name){
            $this->redis->$name;
        } else {
            $this->$name($arguments);
        }
    }
    */
}