<?php

namespace imagekeeper\components;

use imagekeeper\controllers\HttpController;

class UrlRouter
{

    /**
     * @var Logger
     */
    private $log;

    /**
     * @var array
     */
    private $routes;

    public function __construct()
    {
        $this->routes = require(App::getInstance()->getConfig()['project_path'] . '/backend/config/Routing.php');
        $this->log = App::getInstance()->getLogger();
    }

    /**
     * @param string $url
     */
    public function route(string $url){
        $controllerClass = null;
        foreach ($this->routes['static'] as $route=>$controller){
            if($route === $url){
                $controllerClass = new $controller;
                $controllerClass->setRoute($url);
                break;
            }
        }
        if (empty($controllerClass)) {
            $this->log->debug("No static route found for `" . $url . "`");

            foreach ($this->routes['regexp'] as $regexpRoute => $controller){
                if(preg_match($regexpRoute, $url, $result)){

                    $this->log->debug("Found regexp route matching `" . $url . "` - " . json_encode($result, JSON_PRETTY_PRINT));

                    array_shift($result);

                    $controllerClass = new $controller;
                    $controllerClass->setRoute($url, $result);
                    break;
                }
            }

        }

        if (empty($controllerClass)) {
            $this->log->debug("No regexp route found for `" . $url . "`");
            http_response_code(404);
        } else {
            $controllerClass->execute();
        }

    }

}
