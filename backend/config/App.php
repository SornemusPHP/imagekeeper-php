<?php 

return [
    'project_name' => 'imagekeeper',
    'project_path' => '/vagrant',
    'collectionPath' => '/vagrant/web/images',
    'frontend_path' => '/vagrant/web',
    'logger' => [
        'file' => [
            'path' => '/vagrant/logs/app-imagekeeper.log',
            'format' => '[%TIME%] --- %LEVEL% --- %MESSAGE%',//'[%TIMESTAMP%] %CONTEXT% --- %LEVEL% --- %MESSAGE%',
            'level' => '',
        ],
        'mongo' => [
            'collection' => 'logs',
            'level' => '',
        ]
    ],


    'data_sources' => [

    ],



];
