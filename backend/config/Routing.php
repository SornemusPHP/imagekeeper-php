<?php

$routes = [
    'static' => [
            //SYSTEM
            'sys/phpinfo' => \imagekeeper\controllers\system\PhpInfo::class,
            'sys/test' => \imagekeeper\controllers\system\Test::class,

            'sys/session' => \imagekeeper\controllers\system\ViewSession::class,

            'sys/redis' => \imagekeeper\controllers\system\RedisAdmin::class,
            'sys/mongodb' => \imagekeeper\controllers\system\MongoAdmin::class,

            'sys/logs' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/debug' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/info' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/warn' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/error' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/debug+' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/info+' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/warn+' => \imagekeeper\controllers\system\ViewMongoLogs::class,
            'sys/logs/error+' => \imagekeeper\controllers\system\ViewMongoLogs::class,

            'sys/rest/log' => \imagekeeper\controllers\system\RestLogger::class,
            'sys/rest/log/clear' => \imagekeeper\controllers\system\RestLogger::class,


            'sys/rest/RestTest' => \imagekeeper\controllers\system\RestTest::class,

            //KEEPER
            '' => \imagekeeper\controllers\Index::class,
            'view' =>\imagekeeper\controllers\PictureView::class,
            'rest/IndexPictures' => \imagekeeper\controllers\RestIndexPictures::class,
        ],
    'regexp' => [
            '/^(view)\/(\d*)$/' => \imagekeeper\controllers\PictureView::class,
            '/^(images)\/(mongo)\/(\d*)$/' => \imagekeeper\controllers\MongoImage::class,
        ]
];

return $routes;