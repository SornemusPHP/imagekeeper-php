<?php

namespace imagekeeper\controllers;

use imagekeeper\components\App;

abstract class HttpController
{
    /**
     * @var \imagekeeper\components\Logger
     */
    protected $log;


    /**
     * @var \imagekeeper\components\Session
     */
    protected $session;


    /**
     * @var string
     */
    protected $route;

    /**
     * @var array
     */
    protected $urlParts;

    public function __construct()
    {
        $this->session = App::getInstance()->getSession();
        $this->log = App::getInstance()->getLogger();
    }

    public function execute(){
        $toExecute = 'http' . ucfirst(strtolower($_SERVER['REQUEST_METHOD']));



        $this->log->debug("''" . $toExecute .
            "' in controller '" . (new \ReflectionClass($this))->getShortName() .
            "' with route '" . $this->route . "'");



        $this->before();
        $this->$toExecute();
        $this->after();
    }
    //================
    public function before(){
        //...
    }
    //================
    public function httpGet(){
        //...
    }
    public function httpHead(){
        //...
    }
    public function httpPost(){
        //...
    }
    public function httpPut(){
        //...
    }
    public function httpDelete(){
        //...
    }
    public function httpTrace(){
        //...
    }
    public function httpOptions(){
        //...
    }
    public function httpConnect(){
        //...
    }
    public function httpPath(){
        //...
    }
    //================
    public function after(){
        //...
    }
    //================
    public function setRoute(string $route, array $regexpParts = []){
        $this->route = $route;
        if (!empty($regexpParts)){
            $this->urlParts = $regexpParts;
        } else {
            $this->urlParts = explode('/', $route);
        }
    }
}
