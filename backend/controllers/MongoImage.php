<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 11.04.17
 * Time: 14:01
 */

namespace imagekeeper\controllers;


use imagekeeper\components\App;

class MongoImage extends HttpController
{
    public function httpGet()
    {
        $id = $this->urlParts[2];
        
        $redis = App::getInstance()->getRedis();
        $redis->connect('127.0.0.1');
        
        
        $mongo = App::getInstance()->getMongoDb();
        $bucket = $mongo->selectGridFSBucket();
    
    
        
        $this->log->info('Getting image from MongoDB by RedisId: ' . $redis->get("Pictures:{$id}:FullPath"));
        $fullPath = $redis->get("Pictures:{$id}:FullPath");
        $redis->close();


        header('Content-Type: image/png');
        $out = fopen("php://output", 'w');
        $bucket->downloadToStreamByName($fullPath, $out);


    
    
    }
}