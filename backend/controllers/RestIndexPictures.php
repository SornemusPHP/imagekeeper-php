<?php

namespace imagekeeper\controllers;


use imagekeeper\components\App;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class RestIndexPictures extends RestController
{
    public function httpPost()
    {
        $collectionPath = App::getInstance()->getConfig()['collectionPath'];

        //$path = realpath($collectionPath);

        $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($collectionPath));
        $files = [];
        foreach($objects as $name => $object){
            $filePath = $name;
            $relPath = str_replace($collectionPath . '/', '', $name);
            $fileName = explode('/', $filePath);
            $fileName = end($fileName);

            if($fileName != '.' && $fileName != '..'){
                $files[] = [
                    'name' => $fileName,
                    'path' => $relPath,
                    'full_path' => $filePath,
                ];
            }
        }


        $this->log->info('Indexing pictures folder to Redis');

        $redis = App::getInstance()->getRedis();
        $redis->connect('127.0.0.1');

        $redis->del($redis->keys('Pictures*'));


        $redis->set('Pictures:FromId', 1000001);
        $redis->set('Pictures:ToId', 1000000);
        $id = 0;
    
        $mongo = App::getInstance()->getMongoDb();
        $bucket = $mongo->selectGridFSBucket();
        
        foreach ($files as $oneFile) {

            $id = $redis->incr('Pictures:ToId');

            $redis->set('Pictures:' . $id . ':FullPath', $oneFile['full_path']);
            $redis->set('Pictures:' . $id . ':WebPath', $oneFile['path']);

            $base64 = file_get_contents($oneFile['full_path']);
            $redis->set('Pictures:' . $id . ':Base64', base64_encode($base64));
    
            
            $file = fopen($oneFile['full_path'], 'rb');
            $res = $bucket->uploadFromStream($oneFile['full_path'], $file);
            
    
            $this->log->info('Added picture to GridFS with id: ' . $res);
    
    
            $redis->set('Pictures:' . $id . ':MongoDbId', $res . '');


            
        }
        $this->log->info('Indexed ' . ($id - 1000000) . ' pictures');
        $redis->close();

        //$this->responseData['filesList'] = $fileNames;
    }
}