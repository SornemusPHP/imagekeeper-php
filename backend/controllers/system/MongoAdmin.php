<?php

namespace imagekeeper\controllers\system;


use imagekeeper\components\App;
use imagekeeper\controllers\HttpController;
use imagekeeper\views\HtmlView;

class MongoAdmin extends HttpController
{
    public function httpGet()
    {

        $view = new HtmlView('MongoDB Admin');


        $view->addCommon();
        $view->addNavBar();


        $view->bodyAppend('<div class="container">');


        exec('du -h -c /var/lib/mongodb/*', $result);
        exec('du -h -c /vagrant/logs/mongodb.log', $result);
        $view->bodyAppend('<div class="panel panel-default">');
        $view->bodyAppend('<div class="panel-heading"><h3 class="panel-title"><strong><u>
        Disk space usage
        </u></strong></h3></div>');
        $view->bodyAppend('<div class="panel-body">');
        $view->bodyAppend($result);
        $view->bodyAppend('</div></div>');


        $view->bodyAppend('<div class="panel panel-default">');
        $view->bodyAppend('<div class="panel-heading"><h3 class="panel-title"><strong><u>
        Data
        </u></strong></h3></div>');


        $mongo = App::getInstance()->getMongoDb();
        $collections = $mongo->listCollections();

        $view->bodyAppend('<div class="panel-body">');
        foreach($collections as $collection){
            $collName = $collection->getName();
            $view->bodyAppend('<h4>' . $collName .
                                ' <span class="badge">' . $mongo->$collName->count() . '</span> </h4>');
            $collDocs = $mongo->$collName->find([], ['limit' => 50])->toArray();
            $view->bodyAppend('<ul>');
            foreach($collDocs as $document){
                $view->bodyAppend('<li>' . $document['_id'] . '</li>');
            }
            $view->bodyAppend('</ul>');

        }
        $view->bodyAppend('</div></div>');

        $view->bodyAppend('</div>');
        $view->renderView();

    }
}