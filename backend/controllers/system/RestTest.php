<?php

namespace imagekeeper\controllers\system;


use imagekeeper\controllers\RestController;

class RestTest extends RestController
{
    public function httpGet()
    {
        $respData = [
            'responseone' => 'foo',
            'responsetwo' => 'bar',
        ];

        $this->responseData = $respData;
    }

    public function httpPost()
    {

        $this->log->debug("Received " . json_encode($this->requestData));



        $respData = [
            'responseone' => 'foo',
            'responsetwo' => 'bar',
        ];
        $this->log->debug("Sent back " . json_encode($respData));

        $this->responseData = $respData;
    }
}