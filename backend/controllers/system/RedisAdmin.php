<?php

namespace imagekeeper\controllers\system;


use imagekeeper\components\App;
use imagekeeper\controllers\HttpController;
use imagekeeper\views\HtmlView;

class RedisAdmin extends HttpController
{
    public function httpGet()
    {
        $redis = App::getInstance()->getRedis();

        $redis->connect('127.0.0.1');

        $keys = $redis->keys('*');
        sort($keys);
        $values = $redis->getMultiple($keys);

        $view = new HtmlView('Redis Admin');


        $view->addCommon();

        $view->addNavBar();

        $view->bodyAppend('<div class="container">');

/*
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Panel title</h3>
  </div>
  <div class="panel-body">
    Panel content
  </div>
</div>
 */

        exec('du -h -c /var/lib/redis/*', $result);
        exec('du -h -c /var/log/redis/*', $result);
        $view->bodyAppend('<div class="panel panel-default">');
        $view->bodyAppend('<div class="panel-heading"><h3 class="panel-title"><strong><u>
        Disk space usage
        </u></strong></h3></div>');
        $view->bodyAppend('<div class="panel-body">');
        $view->bodyAppend($result);
        $view->bodyAppend('</div></div>');


        $view->bodyAppend('<div class="panel panel-default">');
        $view->bodyAppend('<div class="panel-heading"><h3 class="panel-title"><strong><u>
        Data
        </u></strong></h3></div>');
        $view->bodyAppend('<table class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th class="col-md-3">Key</td>
                        <th class="col-md-6">Value</td>
                        <th class="col-md-3">Actions</td>
                    </tr>
                </thead>
                <tbody>');




        for($i = 0; $i < count($keys); $i++){
            $line = '<tr><td>' . $keys[$i] . '</td><td>' . substr($values[$i], 0, 50) . '</td><td>actions</td></tr>';

            $view->bodyAppend($line);
        }
        $view->bodyAppend('</tbody>
</table>
</div>
</div>
    ');


        $view->renderView();

        $redis->close();
    }

}