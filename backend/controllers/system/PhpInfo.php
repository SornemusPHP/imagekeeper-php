<?php

namespace imagekeeper\controllers\system;


use imagekeeper\controllers\HttpController;

class PhpInfo extends HttpController
{
    public function httpGet()
    {
        phpinfo();
    }
}