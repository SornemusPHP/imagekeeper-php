<?php

namespace imagekeeper\controllers\system;


use imagekeeper\controllers\HttpController;

class Test extends HttpController
{
    public function httpGet()
    {
        echo "test controller";
    }
}