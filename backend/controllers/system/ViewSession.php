<?php

namespace imagekeeper\controllers\system;


use imagekeeper\components\App;
use imagekeeper\controllers\HttpController;

class ViewSession extends HttpController
{
    public function httpGet()
    {
        header('Content-Type: application/json');
        echo (json_encode(App::getInstance()->getSession()->getAll(), JSON_PRETTY_PRINT));
    }

}