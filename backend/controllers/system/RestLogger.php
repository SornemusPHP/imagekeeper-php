<?php

namespace imagekeeper\controllers\system;

use imagekeeper\components\App;
use imagekeeper\controllers\RestController;

class RestLogger extends RestController
{
    public function httpGet()
    {
        $urlParts = explode('/', $this->route);
        if(isset($urlParts[3])) {
            if($urlParts[3] === 'clear'){
                $collectionName = App::getInstance()->getConfig()['logger']['mongo']['collection'];
                $collection = App::getInstance()->getMongoDb()->$collectionName;
                $collection->deleteMany([]);
                header('Location: /sys/logs');
            }
        }
    }

    public function httpPost()
    {
        $message = "[REST] " . $this->requestData['message'];
        $this->log->info($message);
    }
}
