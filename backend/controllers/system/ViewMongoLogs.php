<?php

namespace imagekeeper\controllers\system;


use imagekeeper\components\App;
use imagekeeper\controllers\HttpController;
use imagekeeper\views\HtmlView;

class ViewMongoLogs extends HttpController
{

    public function httpGet()
    {
        $view = new HtmlView('Mongo Logs');

        $view->addCommon();

        $view->addNavBar();


        $config = App::getInstance()->getConfig();
        $collectionName = $config['logger']['mongo']['collection'];


        $levels = [];

        if(isset(explode('/', $this->route)[2])) {
            $displayLevel = strtoupper(explode('/', $this->route)[2]);
            $plus = substr($displayLevel, -1);
            if($plus === '+'){
                switch ($displayLevel){
                    case "DEBUG+":
                        $levels[] = 'DEBUG';
                    case "INFO+":
                        $levels[] = 'INFO';
                    case "WARN+":
                        $levels[] = 'WARN';
                    case "ERROR+":
                        $levels[] = 'ERROR';
                        break;
                }
            } else {
                $levels[] = $displayLevel;
            }
        }

        $query = [];
        if(!empty($levels)){
            $query = [
                'level' => ['$in' => $levels]
            ];
        }


        $collection = App::getInstance()->getMongoDb()->$collectionName;
        $cursorArr = $collection->find(
            $query,
            [
                'sort' => ['time' => -1],
                'limit' => 50
            ])->toArray();
        $allCount = $collection->count();
        $foundCount = count($cursorArr);
        $view->bodyAppend('
        <h4>
Entry count in \'' . $collectionName . '\' collection: <span class="badge">' . $allCount . '</span>
</h4>
<h4>
Found entries: <span class="badge">' . $foundCount . '</span>
</h4>
<h3>
<a href="/sys/logs"><span class="label label-primary">ALL</span></a>
</h3>
<h3>
<a href="/sys/logs/debug"><span class="label label-info">DEBUG</span></a> 
<a href="/sys/logs/debug+"><span class="label label-info">DEBUG+</span></a>
</h3>
<h3>
<a href="/sys/logs/info"><span class="label label-info">INFO</span></a>
<a href="/sys/logs/info+"><span class="label label-info">INFO+</span></a>
</h3>
<h3>
<a href="/sys/logs/warn"><span class="label label-info">WARN</span></a>
<a href="/sys/logs/warn+"><span class="label label-info">WARN+</span></a>
</h3>
<h3>
<a href="/sys/logs/error"><span class="label label-info">ERROR</span></a>
<a href="/sys/logs/error+"><span class="label label-info">ERROR+</span></a>
</h3>
<h3>
<a href="/sys/rest/log/clear"><span class="label label-danger">CLEAR LOGS</span></a>
</h3>
        <br>
        <br>
        ');


        $view->bodyAppend('
        <table class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="col-md-2">_ID</td>
                        <th class="col-md-2">Time</td>
                        <th class="col-md-2">Context</td>
                        <th class="col-md-2">Level</td>
                        <th class="col-md-4">Message</td>
                    </tr>
                </thead>
                <tbody>
        ');

        foreach ($cursorArr as $document) {
            $view->bodyAppend('<tr>' .
                '<td>' . $document['_id'] . '</td>' .
                '<td>' . $document['time'] . '</td>' .
                '<td>' . $document['context'] . '</td>' .
                '<td>' . $document['level'] . '</td>' .
                '<td>' . $document['message'] . '</td>' .
                '</tr>');
        }


        $view->bodyAppend('</tbody>
</table>>');

        $view->renderView();
    }
}