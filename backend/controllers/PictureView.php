<?php

namespace imagekeeper\controllers;


use imagekeeper\components\App;
use imagekeeper\views\HtmlView;

class PictureView extends HttpController
{
    public function httpGet()
    {
        $view = new HtmlView('View Picture');
        $view->addCommon();
        

        $redis = App::getInstance()->getRedis();
        $redis->connect('127.0.0.1');

        $from = $redis->get('Pictures:FromId');
        $to = $redis->get('Pictures:ToId');

        if(!isset($this->urlParts[1])){
            $view->addBootstrap();



            $view->bodyAppend('<div style="height: 100%">');


            for($i = $from; $i <= $to; $i++){
                $path = $redis->get('Pictures:' . $i . ':FullPath');
                if($path !== false) {
                    $view->bodyAppend('<a href="/view/' . $i . '">' . $path . '</a><br>');
                }
            }
            $view->bodyAppend('</div>');

        } else {
            $id = $this->urlParts[1];
            $prevId = ($id === $from) ? $to : ($id - 1);
            $nextId = ($id === $to) ? $from : ($id + 1);


            $base64 = $redis->get('Pictures:' . $id . ':Base64');
            $webPath = $redis->get('Pictures:' . $id . ':WebPath');

            //$view->bodyAppend('<canvas id="mainCanvas" style="width: 100%; height: 100%; border: solid 1px;"></canvas>');
            //$view->bodyAppend("<img src=\"data:image/png;base64," . $base64 . "\">");
            $view->bodyAppend("<img src=\"/images/$webPath\">");

            $view->footerAppend("
            <script>
            function drawImageScaled(img, ctx) {
               var canvas = ctx.canvas ;
               var hRatio = canvas.width  / img.width    ;
               var vRatio =  canvas.height / img.height  ;
               var ratio  = Math.min ( hRatio, vRatio );
               var centerShift_x = ( canvas.width - img.width*ratio ) / 2;
               var centerShift_y = ( canvas.height - img.height*ratio ) / 2;  
               ctx.clearRect(0,0,canvas.width, canvas.height);
               ctx.drawImage(img, 0,0, img.width, img.height,
                                  centerShift_x,centerShift_y,img.width*ratio, img.height*ratio);  
            }
            
            
            
    function draw(id) {
        //var width = window.innerWidth*0.98;
        //var height = window.innerHeight*0.98;


        var canvas = document.getElementById('mainCanvas');
        var context = canvas.getContext('2d');
        var image = new Image();

        image.src = 'data:image/png;base64,{$base64}';//'/images/mongo/' + id + '/';
        //image.src = '/images/mongo/' + id + '/';
        image.onload = function() {
            /*context.drawImage(image,
                canvas.width / 2 - image.width / 2,
                canvas.height / 2 - image.height / 2,
                canvas.width,
                canvas.height
            );*/
            
            drawImageScaled(image, context);
        };
        
        $.ajax({
            url: '/images/mongo/$id',
            type: 'GET'
        }).done(function (result) {
            l(result);
        });

    }
    
    function init(){
    
        //draw({$id});
        
        $(window).keydown(function(event) {
            
            
            
            if(event.key === 'ArrowLeft'){
                event.preventDefault();
                window.location = '/view/{$prevId}';
            }            
            if(event.key === 'ArrowRight'){
                event.preventDefault();
                window.location = '/view/{$nextId}';
            }
        });
    
    }
    
    
    init();
    
    </script>            
            ");

            //TODO: use redis and base64
            //$view->bodyAppend("<img src=\"data:image/png;base64," . $base64 . "\">");
        }

        $redis->close();
    
    
        //$view->addNavBar();
        $view->addJQuery();
        $view->renderView();

/*
        $mongo = App::getInstance()->getMongoDb();
        $bucket = $mongo->selectGridFSBucket();
        $file = fopen('/vagrant/web/images/apple-touch-icon.png', 'rb');
        $res = $bucket->uploadFromStream('/vagrant/web/images/apple-touch-icon.png', $file);
        //var_dump($res);

        $log->info('Added picture to GridFS with id: ' . $res);

        $out = fopen("php://output", 'w');


        header('Content-Type: image/png');
        $bucket->downloadToStream($res, $out);
*/




        //echo "<img src='./images/newimg.png'/>";

    }
}