<?php
namespace imagekeeper\controllers;


abstract class RestController extends HttpController
{
    protected $requestData;
    protected $responseData;

    public function __construct()
    {
        parent::__construct();

    }

    public function before(){
        if(isset($_POST['json'])) {
            $this->requestData = json_decode($_POST['json'], true);
        }
    }

    public function after(){
        ob_start();
        ob_clean();
        header('Content-Type: application/json');
        if(!empty($this->responseData)) {
            echo(json_encode($this->responseData));
        }
        ob_end_flush();
    }


}
