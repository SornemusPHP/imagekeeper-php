<?php

namespace imagekeeper\controllers;


use imagekeeper\components\App;
use imagekeeper\views\HtmlView;

class Index extends HttpController
{
    public function httpGet(){
        $view = new HtmlView('Home');
        //$index = file_get_contents(App::getInstance()->getConfig()['frontend_path'] .  "/templates/index.html");

        $view->addCommon();

        //$this->log->debug("Displaying index page with lines " . substr_count($index, "\n"));

        $view->addNavBar();
        $view->addActionsBar();
        $view->renderView();
        //echo $index;
    }
}
