<?php

namespace imagekeeper\views;


class HtmlView
{
    const JQUERY = 100;

    /**
     * @var string
     */
    private $mainHtml;

    private $headHtml;
    private $bodyHtml;
    private $footerHtml;

    /**
     * HtmlView constructor.
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->mainHtml = require('templates/masterpage.php');
        $this->mainHtml = str_replace('{{TITLE}}', $title, $this->mainHtml);


        $this->headHtml = '';
        $this->bodyHtml = '';
        $this->footerHtml = '';
    }



    /**
     * @param array|string $append
     */
    public function headAppend($append){
        if(is_array($append)){
            $append = implode("<br>", $append);
        }
        $this->headHtml .= $append;
    }

    /**
     * @param array|string $append
     */
    public function bodyAppend($append){
        if(is_array($append)){
            $append = implode("<br>", $append);
        }
        $this->bodyHtml .= $append;
    }    /**
     * @param array|string $append
     */
    public function footerAppend($append){
        if(is_array($append)){
            $append = implode("<br>", $append);
        }
        $this->footerHtml .= $append;
    }


    public function addCommon(){
        $styles = require('templates/custom_styles.php');
        $scripts = require('templates/custom_scripts.php');
        $this->mainHtml = str_replace('{{HEAD}}',"{{HEAD}}\n"  . $styles . "\n" . $scripts, $this->mainHtml);
    }
    public function addJQuery(){
        $toInclude = require('templates/jquery_cdn.php');
        $this->mainHtml = str_replace('{{FOOTER}}', $toInclude . "\n{{FOOTER}}", $this->mainHtml);
    }
    public function addBootstrap(){
        $toInclude = require('templates/bootstrap.php');
        $this->mainHtml = str_replace('{{HEAD}}', $toInclude['head'] . "\n{{HEAD}}" , $this->mainHtml);
        $this->mainHtml = str_replace('{{FOOTER}}', $toInclude['footer'] . "\n{{FOOTER}}", $this->mainHtml);
    }

    public function addNavBar(){
        $this->addJQuery();
        $this->addBootstrap();

        $toInclude = require('templates/navbar.php');
        $this->bodyAppend($toInclude);
    }
    public function addActionsBar(){
        $toInclude = require('templates/actions_bar.php');
        $this->bodyAppend($toInclude);
    }

    public function renderView(){
        $this->mainHtml = str_replace('{{HEAD}}', $this->headHtml, $this->mainHtml);
        $this->mainHtml = str_replace('{{BODY}}', $this->bodyHtml, $this->mainHtml);
        $this->mainHtml = str_replace('{{FOOTER}}', $this->footerHtml, $this->mainHtml);

        echo($this->mainHtml);
    }
}