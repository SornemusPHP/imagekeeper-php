<?php

return '
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>{{TITLE}}</title>    
        {{HEAD}}
    </head>
    <body>
        {{BODY}}        
    </body>
    <footer>
        {{FOOTER}}        
    </footer>
</html>
';