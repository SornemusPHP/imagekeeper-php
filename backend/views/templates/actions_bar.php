<?php


return '
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-form">
           <button class="btn btn-toolbar" onclick="indexPictures()">Index pictures</button>
        </div>
    </div>
</nav>

<script>
    function indexPictures() {        
        l("indexating");
        $.ajax({
            url: \'/rest/IndexPictures\',
            type: "POST",
            data: {
                //json: JSON.stringify([{\'onedata\':\'foo\'}, {\'otherdata\':\'bar\'}])
            },
            success: function () {
                l("indexated");
            }
        });
    }
</script>
';