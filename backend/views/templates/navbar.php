<?php

/*
return '
<div>
<ul class="for-navbar">
    <li class="for-navbar"><a class="for-navbar" href="/view">Pictures</a></li>
    <li class="for-navbar" style="float:right"><a class="for-navbar" href="/sys/logs">Logs</a></li>
    <li class="for-navbar" style="float:right"><a class="for-navbar" href="/sys/redis">Redis</a></li>
    <li class="for-navbar" style="float:right"><a class="for-navbar" href="/sys/mongodb">MongoDB</a></li>
</ul>
</div>
';*/


return '
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">Home</a>
      <a class="navbar-brand" href="/view">Pictures</a>
    </div>
    
    <ul class="nav navbar-nav navbar-right">
        <li role="presentation" class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" 
          aria-haspopup="true" aria-expanded="false">
          SYSTEM <span class="caret">
          </a>
          <ul class="dropdown-menu">
            <li><a href="/sys/redis">Redis Admin</a></li>
            <li><a href="/sys/mongodb">Mongo Admin</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/sys/logs">Logs</a></li>
          </ul>
        </li>
    </ul>
    
  </div>
</nav>
';